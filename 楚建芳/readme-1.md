### 一.；类型的访问修饰符和修饰符
1.访问修饰符
  +public            公共的，可以被任何代码访问
  +internal          只能在当前项目（它所在的项目）所访问

2.修饰符
  +static            静态的，其它成员必然为静态成员
  +abstract          抽象的，无法被实例化，只能被继承
  +sealed            封闭的，无法被继承

### 二.；类型成员的访问修饰符和修饰符
1.访问修饰符
  +public            公共的，可以被类型内部和外部的代码访问
  +private           私有的，只能被类型内部代码访问
  +internal          内部的，只能被当前项目代码访问
  +protected         保护的，只能被当前类型和派生类型代码访问

2.修饰符
   +字段
       +readonly     公有的，只能读取该字段的值且无法更改该字段的值
       +static       静态的，可以直接通过类名访问该字段，常量不能使用
   
   +方法
       +virtual      虚拟的，在子类中可重写
       +abstract     抽象的，没有方法主体，无法实例化，是一种不完整的类
       +static       静态的
       +sealed       密封的，必须通过重写基类中的虚方法
       +override     重写的，可在类之间继承时使用
              

3.字段的语法形式
访问修饰符 修饰符 数据类型 字段名        修饰符和访问修饰符可省略，可互换位置，在类中定义字段时字段名是唯一的

4.在Test类中分别定义不同修饰符的字段
   private unt id                      定义私有的整型字段id
   public readonly string name         定义公有的只读字符串类型字段 name
   internal static int age             定义内部的静态的整形字段 age
   private const string major = "计算机" 定义私有的字符串类型常量 major 

5.字段在类中定义完成后，在类加载时，会自动为字段赋值，不同数据类型的字段默认值不同
   数据类型          默认值         常用类型
   整数类型            0             int  
   浮点型              0           float double
   字符串类型         空值            string
   字符型              a            char(n)  varchar(n)
   布尔型            False           boolean
   其它引用类型       空值            text
